(function ($) {
    "use strict";

    // Windows load

    $(window).on("load", function () {

        // Site loader 

        $(".loader-inner").fadeOut();
        $(".loader").delay(200).fadeOut("slow");

    });


    // Scroll to

    $('a.scroll').smoothScroll({
        speed: 800,
        offset: -50
    });


    // Slider

    $('.slider').flexslider({
        animation: "fade",
        slideshow: true,
        directionNav: true,
        controlNav: false,
        pauseOnAction: false,
        animationSpeed: 500
    });


    $('.review-slider').flexslider({
        animation: "fade",
        slideshow: true,
        directionNav: false,
        controlNav: true,
        pauseOnAction: false,
        animationSpeed: 1000
    });



    // Mobile menu

    var mobileBtn = $('.mobile-but');
    var nav = $('.main-nav ul');
    var navHeight = nav.height();

    $(mobileBtn).on("click", function () {
        $(".toggle-mobile-but").toggleClass("active");
        nav.slideToggle();
        $('.main-nav li a').addClass('mobile');
        return false;


    });

    $(window).resize(function () {
        var w = $(window).width();
        if (w > 320 && nav.is(':hidden')) {
            nav.removeAttr('style');
            $('.main-nav li a').removeClass('mobile');
        }

    });

    $('.main-nav li a').on("click", function () {
        if ($(this).hasClass('mobile')) {
            nav.slideToggle();
            $(".toggle-mobile-but").toggleClass("active");
        }

    });



    // Append images as css background

    $('.background-img').each(function () {
        var path = $(this).children('img').attr('src');
        $(this).css('background-image', 'url("' + path + '")').css('background-position', 'initial');
    });




    // Count down setup

    $('.countdown').countdown('2018/06/27', function (event) {
        $(this).html(event.strftime('%D days %H:%M:%S'));
    });




    // Tabbed content 

    $(".block-tabs li").on("click", function () {
        if (!$(this).hasClass("active")) {
            var tabNum = $(this).index();
            var nthChild = tabNum + 1;
            $(".block-tabs li.active").removeClass("active");
            $(this).addClass("active");
            $(".block-tab  li.active").removeClass("active");
            $(".block-tab li:nth-child(" + nthChild + ")").addClass("active");
        }
    });


    // Zoom	effect

    $('.block-gallery li').on("mouseenter", function () {
        $(this).closest('.gallery').find('.block-gallery li').removeClass('active');
        $(this).addClass('active');
    });



    $('.block-ticket').on("mouseenter", function () {
        $(this).closest('.tickets').find('.block-ticket').removeClass('active');
        $(this).addClass('active');
    });




    // Images zoom 

    $('.venobox').venobox({
        titleattr: 'data-title',
        numeratio: true
    });




    // Form validation 

    var resgistryForm = $('.registry-form');
    resgistryForm.validate({
        validClass: 'valid',
        errorClass: 'error',
        errorPlacement: function (error, element) {
            return true;
        },
        onfocusout: function (element, event) {
            $(element).valid();
        },
        rules: {
            email: {
                required: true,
                email: true
            }
        },

        rules: {
            name: {
                required: true,
                minlength: 3
            }
        }


    });



})(jQuery);


// Map setup 

function initializeMap() {



    var styles = [{
        "elementType": "geometry",
        "stylers": [{
            "color": "#212121"
        }]
    },
    {
        "elementType": "labels.icon",
        "stylers": [{
            "visibility": "off"
        }]
    },
    {
        "elementType": "labels.text.fill",
        "stylers": [{
            "color": "#757575"
        }]
    },
    {
        "elementType": "labels.text.stroke",
        "stylers": [{
            "color": "#212121"
        }]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry",
        "stylers": [{
            "color": "#757575"
        }]
    },
    {
        "featureType": "administrative.country",
        "elementType": "labels.text.fill",
        "stylers": [{
            "color": "#9e9e9e"
        }]
    },
    {
        "featureType": "administrative.land_parcel",
        "stylers": [{
            "visibility": "off"
        }]
    },
    {
        "featureType": "administrative.locality",
        "elementType": "labels.text.fill",
        "stylers": [{
            "color": "#bdbdbd"
        }]
    },
    {
        "featureType": "poi",
        "elementType": "labels.text.fill",
        "stylers": [{
            "color": "#757575"
        }]
    },
    {
        "featureType": "poi.park",
        "elementType": "geometry",
        "stylers": [{
            "color": "#181818"
        }]
    },
    {
        "featureType": "poi.park",
        "elementType": "labels.text.fill",
        "stylers": [{
            "color": "#616161"
        }]
    },
    {
        "featureType": "poi.park",
        "elementType": "labels.text.stroke",
        "stylers": [{
            "color": "#1b1b1b"
        }]
    },
    {
        "featureType": "road",
        "elementType": "geometry.fill",
        "stylers": [{
            "color": "#2c2c2c"
        }]
    },
    {
        "featureType": "road",
        "elementType": "labels.text.fill",
        "stylers": [{
            "color": "#8a8a8a"
        }]
    },
    {
        "featureType": "road.arterial",
        "elementType": "geometry",
        "stylers": [{
            "color": "#373737"
        }]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry",
        "stylers": [{
            "color": "#3c3c3c"
        }]
    },
    {
        "featureType": "road.highway.controlled_access",
        "elementType": "geometry",
        "stylers": [{
            "color": "#4e4e4e"
        }]
    },
    {
        "featureType": "road.local",
        "elementType": "labels.text.fill",
        "stylers": [{
            "color": "#616161"
        }]
    },
    {
        "featureType": "transit",
        "elementType": "labels.text.fill",
        "stylers": [{
            "color": "#757575"
        }]
    },
    {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [{
            "color": "#000000"
        }]
    },
    {
        "featureType": "water",
        "elementType": "labels.text.fill",
        "stylers": [{
            "color": "#3d3d3d"
        }]
    }
    ],

        lat = 6.426495,
        lng = 3.430747,




        customMap = new google.maps.StyledMapType(styles, {
            name: 'Styled Map'
        }),


        mapOptions = {
            zoom: 14,
            scrollwheel: false,
            disableDefaultUI: true,
            draggable: true,
            center: new google.maps.LatLng(lat, lng),
            mapTypeControlOptions: {
                mapTypeIds: [google.maps.MapTypeId.ROADMAP]
            }
        },
        map = new google.maps.Map(document.getElementById('map'), mapOptions),
        myLatlng = new google.maps.LatLng(lat, lng),




        marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            icon: {
                url: 'img/marker.png',
                scaledSize: new google.maps.Size(80, 80)
            }
        });




    map.mapTypes.set('map_style', customMap);
    map.setMapTypeId('map_style');



    var transitLayer = new google.maps.TransitLayer();
    transitLayer.setMap(map);



}

$("#second-attendee").click(function () {
    $("#attendee2-div").removeClass("hide");
    $(this).addClass("hide");
})
$("#third-attendee").click(function () {
    $("#attendee3-div").removeClass("hide");
    $(this).addClass("hide");
})

document.getElementById("exhibit_btn").addEventListener('click', () => {
    let form_exhibit = document.getElementById("exhibit-sheet");
    let form_registration = document.getElementById("registration_form");
    form_exhibit.classList.remove("hide");
    form_registration.classList.add("hide")

})


//Registration Setup
const scriptURL = 'https://script.google.com/macros/s/AKfycbwPQrazN0UN-Ys-vG-UArBI_klT1bAHu9_hyT74c87YybCbTRI/exec'


const form = document.forms['submit-to-google-sheet'];
// const form1 = document.forms['exhibit-sheet'];
const buttonSpinnerAction = document.getElementsByClassName('btn-spinner')[0];
const buttonText = document.getElementsByClassName('btn-inner-text')[0]
//document.getElementById('submit_btn').addEventListener("click", confirmation_form);
let emailText;
function isValid() {
    var forms = document.getElementsByClassName('forms');
    for (var i = 0; i < forms.length; i++) {
        if (forms[i].value === "") {
            return false;
        }
    }
    emailText = document.getElementById("company_email").value;
    if (!validateEmail(emailText)) {
        swal("Sorry!!", "Enter a valid email address", "warning");
        return false;
    }

    return true;
}

function validateEmail(email) {
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
}
form.addEventListener('submit', e => {
    e.preventDefault()
    if (!isValid()) {
        return;
    }
    buttonText.classList.add("hidden");
    document.getElementsByClassName('btn-spinner')[0].classList.remove("hidden")
    var attendees = [{
        attendee_surname: $('#attendee_surname1').val(),
        attendee_firstname: $('#attendee_firstname1').val() || "Nil",
        attendee_position: $('#attendee_position1').val() || "Nil",
        attendee_department: $('#attendee_department1').val() || "Nil",
        attendee_email: $('#attendee_email1').val() || "Nil",
        attendee_phonenumber: $('#attendee_phonenumber1').val() || "Nil"
    },
    {
        attendee_surname: $('#attendee_surname2').val(),
        attendee_firstname: $('#attendee_firstname2').val() || "Nil",
        attendee_position: $('#attendee_position2').val() || "Nil",
        attendee_department: $('#attendee_department2').val() || "Nil",
        attendee_email: $('#attendee_email2').val() || "Nil",
        attendee_phonenumber: $('#attendee_phonenumber2').val() || "Nil"
    },
    {
        attendee_surname: $('#attendee_surname3').val(),
        attendee_firstname: $('#attendee_firstname3').val() || "Nil",
        attendee_position: $('#attendee_position3').val() || "Nil",
        attendee_department: $('#attendee_department3').val() || "Nil",
        attendee_email: $('#attendee_email3').val() || "Nil",
        attendee_phonenumber: $('#attendee_phonenumber3').val() || "Nil"
    }];
    postAttendee(attendees[2],
        postAttendee(attendees[1],
            postAttendee(attendees[0], showSuccessMessage, showErrorMessage)
            , showErrorMessage)
        , showErrorMessage);
})

function postAttendee(attendee_data, success_callback, error_callback) {
    var form_data = {
        name_of_organization: $('#name_of_organization').val() || "Nil",
        company_email: $('#company_email').val() || "Nil",
        street: $('#street').val() || "Nil",
        city: $('#city').val() || "Nil",
        state: $('#state').val() || "Nil",
        country: $('#country').val() || "Nil",
        website: $('#website').val() || "Nil",
        sector: $('#sector').val() || "Nil"
    };
    if (attendee_data && attendee_data.attendee_surname) {
        $.extend(form_data, attendee_data);
        var toPostData = new FormData();
        Object.keys(form_data).forEach(key => toPostData.append(key, form_data[key]));
        fetch(scriptURL, { method: 'POST', body: toPostData })
            .then(response => {
                success_callback(response)
            })
            .catch(error => error_callback(error))
    }
    else success_callback()
}

function emailSender() {
    let email = document.getElementById('company_email').value;
    let name = $('#name_of_organization').val();
    $.ajax({
        type: "POST",
        url: "https://email-send-nerc.herokuapp.com",
        data: {
            name: name,
            email: email
        }
    });
}
function showSuccessMessage(response) {
    document.getElementsByClassName('btn-spinner')[0].classList.add("hidden")
    buttonText.classList.remove("hidden");
    // buttonText.innerText = "Successful Registration"
    emailSender();
    swal("Registration completed successfully", "We would be expecting you soon", "success");

    form.reset();


}
function showErrorMessage(error) {
    console.log('Error!', error)
    swal("Error", error, "error");

    setTimeout(() => {
        document.getElementsByClassName('btn-spinner')[0].classList.add("hidden");
        buttonText.classList.remove("hidden");
        buttonText.innerText = "Check your internet connection"
    }, 500)
}

//Registration Setup For Exhibition
const scriptURL1 = "https://script.google.com/macros/s/AKfycbwvT5-YL5lKpdYGGDcjhd9Yc5kA7pS5n0lKG7lWk9LMEhGegDRl/exec"
const form_exhi = document.forms['exhibit-sheet'];
let emailText1;
const buttonSpinnerAction1 = document.getElementsByClassName('btn-spinner1')[0];
const buttonText1 = document.getElementsByClassName('btn-inner-text1')[0]
function isValid1() {
    var forms1 = document.getElementsByClassName('req');

    if (!validateEmail(emailText1)) {
        swal("Sorry!!", "Enter a valid email address", "warning");
        return false;
    }

    return true;
}


form_exhi.addEventListener('submit', e => {
    e.preventDefault()

    emailText1 = document.getElementById("contact_email").value;    
    if(emailText1 === ""){
        swal("Sorry!!", "Enter a valid email address", "warning");        
    }
    else{
        payWithPaystack()
        // buttonText1.classList.add("hidden");
        buttonText1.innerText = "Loading..."        
        fetch(scriptURL1, { method: 'POST', body: new FormData(form_exhi) })
            .then(response => {
                showSuccessMessage(response)
    
            })
            .catch(error => showErrorMessage(error))
    }

})


function payWithPaystack() {
    var handler = PaystackPop.setup({
        key: 'pk_live_8daea009e4d3a78bb97c819c64f53151149823c1',
        email: emailText1,
        amount: 50000000,
        ref: '' + Math.floor((Math.random() * 1000000000) + 1), // generates a pseudo-unique reference. Please replace with a reference you generated. Or remove the line entirely so our API will generate one for you
        metadata: {
            custom_fields: [
                {
                    display_name: "Mobile Number",
                    variable_name: "mobile_number",
                    value: "+2348012345678"
                }
            ]
        },
        callback: function (response) {
            emailSender();
            swal("Successful Registration.", "Check your inbox for your transaction refrence number", "success");
            buttonText1.innerText = "Thank You!!!" 
        },
        onClose: function () {
            emailSender();
        }
    });
    handler.openIframe();
}